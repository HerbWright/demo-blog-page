
export const tryLogin = (username, password) => {
  if(username === 'test' && password == 'test') {
    localStorage.setItem('isLoggedIn', 'true');
    return true;
  }
  return false;
}

export const isLoggedIn = () => {
  return localStorage.getItem('isLoggedIn') === 'true';
}

export const logout = () => {
  localStorage.setItem('isLoggedIn', 'false');
}
