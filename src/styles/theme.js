export const theme = {
  background: '#ffe0e0',
  secondaryBackground: '#FFC0CB',
  boxShadow: '15px 15px 0px #00000030',
  boxShadowSmall: '0px 5px 0px #00000030',
  fontColor: '#502010',
  warningFontColor: '#e00000',
  spacingUnit: '2.5rem',
  clickable: {
    background: '#804132',
    fontColor: '#d0d0d0',
  },
}