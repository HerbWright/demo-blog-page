import './App.css';
import React, { useEffect } from 'react';
import { Routes, Route, useNavigate } from 'react-router-dom';
import LoginPage from './pages/LoginPage';
import BlogsPage from './pages/BlogsPage';
import { ThemeProvider } from 'styled-components';
import { theme } from './styles/theme';
import { isLoggedIn } from './services/LoginApi';



function App() {
  const navigate = useNavigate();
  useEffect(() => {
    if(!isLoggedIn()) {
      navigate('/login');
    }
  }, []);


  return (
    <ThemeProvider theme={theme}>
      <Routes>
        <Route path='/' element={<BlogsPage/>}/>
        <Route path='/login' element={<LoginPage/>}/>
      </Routes>
    </ThemeProvider>
  );
}

export default App;
