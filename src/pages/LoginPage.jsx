import React from 'react'
import CenteredLayout from '../layouts/CenteredLayout';
import LoginCard from '../components/LoginCard';

function LoginPage() {
  return ( 
    <CenteredLayout>
      <LoginCard/>
    </CenteredLayout>
  );
}

export default LoginPage;