import React from 'react'
import BlogList from '../components/BlogList';
import Navbar from '../components/Navbar';
import ListLayout from '../layouts/ListLayout';
import Button from '../components/Button';
import { logout } from '../services/LoginApi';

function BlogsPage() {
  const navBar = <Navbar title='Blogs Page'/>

  return ( 
    <ListLayout navBar={navBar}>
      <BlogList/>
    </ListLayout>
  );
}

export default BlogsPage;