import { ThemeProvider } from "styled-components";
import React from 'react';
import { theme } from "./styles/theme";
import { Router, BrowserRouter } from "react-router-dom";

export const TestWrapperNoRouter = ({children}) => 
  <ThemeProvider theme={theme}>
    {children}
  </ThemeProvider>

export const TestWrapper = ({children}) => 
  <BrowserRouter>
    <TestWrapperNoRouter>
      {children}
    </TestWrapperNoRouter>
  </BrowserRouter>

