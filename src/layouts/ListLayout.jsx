import React from 'react';
import styled from 'styled-components';

const Wrapper = styled.div`
  width: 100vw;
  min-height: 100vh;
  overflow-x: clip;
  display: grid;
  grid-template-columns: auto auto;
  align-items: center;
  justify-items: center;
  justify-content: center;
  flex-direction: column;
  align-items: center;
  padding: calc(${props => props.theme.spacingUnit} * 2);
  padding-top: calc(${props => props.theme.spacingUnit} * 4);
  background: ${props => props.theme.background};
  color: ${props => props.theme.fontColor};
  box-sizing: border-box;
  column-gap: ${props => props.theme.spacingUnit};
  row-gap: ${props => props.theme.spacingUnit};
  @media only screen and (max-width: 1000px) {
    grid-template-columns: auto;
  }
`

function ListLayout({children, navBar}) {
  return (
    <>
      {navBar} 
      <Wrapper>
        {children}
      </Wrapper>
    </>
  );
}

export default ListLayout;