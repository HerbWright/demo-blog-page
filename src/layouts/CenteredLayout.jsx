import React from 'react'
import styled, {useTheme} from 'styled-components';

const Wrapper = styled.div`
  height: 100vh;
  width: 100vw;
  display: flex;
  justify-content: center;
  align-items: center;
  background: ${props => props.theme.background};
  color: ${props => props.theme.fontColor};
`

function CenteredLayout({children}) {
  useTheme();
  return ( 
    <Wrapper>
      {children}
    </Wrapper>
  );
}

export default CenteredLayout;