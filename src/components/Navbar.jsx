import React from 'react'
import styled, {useTheme} from 'styled-components';
import Button from './Button';
import { logout } from '../services/LoginApi';
import { useNavigate } from 'react-router';


const NavbarWrapper = styled.div`
  background: linear-gradient(90deg, ${props => props.theme.secondaryBackground + 'ff'},  ${props => props.theme.secondaryBackground + '00'});
  color: ${props => props.theme.fontColor};
  display: flex;
  width: 100vw;
  box-sizing: border-box;
  column-gap: ${props => props.theme.spacingUnit};
  position: fixed;
  left: 0;
  top: 0;
  padding: calc(${props => props.theme.spacingUnit} * 0.5) calc(${props => props.theme.spacingUnit} * 2);
  align-items: center;
`

const TitleWrapper = styled.div`
  flex-grow: 1;
  font-size: 3rem;
  font-weight: bold;
`

function Navbar({children, title}) {
  const navigate = useNavigate();
  const onClick = () => {
    logout();
    navigate('/login');
  }
  return ( 
    <NavbarWrapper>
      <TitleWrapper>{title}</TitleWrapper>
      {children}
      <Button onClick={onClick}>Log Out</Button>
    </NavbarWrapper>
  );
}

export default Navbar;