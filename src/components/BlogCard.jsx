import React from 'react';
import Card from './Card';

const responsiveWidthStyle = `
  @media only screen and (max-width: 1500px) {
    width: 400px;
  }
`;

function BlogCard({blog}) {
  return ( 
    <Card width="500px" header={blog.title}>
      {blog.body}
    </Card>
  );
}

export default BlogCard;