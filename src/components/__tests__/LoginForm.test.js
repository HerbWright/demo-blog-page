import { screen, render } from "@testing-library/react";
import React from 'react';
import LoginForm from "../LoginForm";
import {TestWrapper, TestWrapperNoRouter} from '../../testUtils';
import userEvent from "@testing-library/user-event";
import * as loginApi from '../../services/LoginApi';
import { Routes, Route, Navigate, MemoryRouter } from 'react-router-dom';

test('LoginForm displays error', () => {
  render(<LoginForm/>, {wrapper: TestWrapper});
  userEvent.click(screen.getByRole('button'));
  expect(screen.queryByText(/incorrect/));
});

test('LoginForm redirects with successful login', async () => {
  render(
    <MemoryRouter initialEntries={['/login']}>
      <Routes>
        <Route path='/' element={<p>successfulRedirect</p>}/>
        <Route path='/login' element={<LoginForm/>}/>
      </Routes>
    </MemoryRouter>,
    {wrapper: TestWrapperNoRouter}
  );
  userEvent.type(screen.getByRole('textbox'), 'test');
  userEvent.type(screen.getByLabelText(/password/i), 'test');
  userEvent.click(screen.getByRole('button'));
  await screen.findByText('successfulRedirect');
});

test('LoginForm calls login with inputs', () => {
  loginApi.tryLogin = jest.fn();
  render(<LoginForm/>, {wrapper: TestWrapper});
  userEvent.type(screen.getByRole('textbox'), 'someUsername');
  userEvent.type(screen.getByLabelText(/password/i), 'somePassword');
  userEvent.click(screen.getByRole('button'));
  expect(loginApi.tryLogin).toHaveBeenCalledWith('someUsername', 'somePassword');
});



