import { screen, render } from "@testing-library/react";
import React from 'react';
import Card from "../Card";
import {TestWrapper} from '../../testUtils';

test('Card displays header', () => {
  render(
    <Card header='someHeader'/>,
    {wrapper: TestWrapper}
  );
  expect(screen.queryByText('someHeader')).toBeInTheDocument();
});

test('Card displays children', () => {
  render(
    <Card><p>someText</p></Card>,
    {wrapper: TestWrapper}  
  );
  expect(screen.queryByText('someText')).toBeInTheDocument();
});


