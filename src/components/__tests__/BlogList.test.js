import React from 'react';
import { screen, render, waitFor } from '@testing-library/react';
import BlogList from '../BlogList';
import {TestWrapper} from '../../testUtils';

jest.mock('../../services/BlogsApi',() => ({
  getAllBlogs: () => [
    {
      title: 'someTitle',
      body: 'someBody',
    },
    {
      title: 'anotherTitle',
      body: 'anotherBody',
    },
  ]
}));

test('BlogList displays data returned from getAllBlogs', async () => {
  render(<BlogList/>, {wrapper: TestWrapper});
  await screen.findByText('someTitle');
  await screen.findByText('someBody');
  await screen.findByText('anotherTitle');
  await screen.findByText('anotherTitle');
});




