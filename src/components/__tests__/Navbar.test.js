import React from 'react';
import { screen, render } from '@testing-library/react';
import Navbar from '../Navbar';
import {TestWrapper, TestWrapperNoRouter} from '../../testUtils';
import userEvent from '@testing-library/user-event';
import { logout } from '../../services/LoginApi';
import {Routes, Route, MemoryRouter} from 'react-router-dom';

jest.mock('../../services/LoginApi');

test('Logout button calls logout function', () => {
  render(<Navbar/>, {wrapper: TestWrapper});
  expect(logout).not.toHaveBeenCalled();
  userEvent.click(screen.getByRole('button', {name: /log( )*out/i}));
  expect(logout).toHaveBeenCalled();
});

test('Logout redirects to login page', async () => {
  render(
    <MemoryRouter initialEntries={['/']}>
      <Routes>
        <Route path='/' element={<Navbar/>}/>
        <Route path='/login' element={<p>successfulRedirect</p>}/>
      </Routes>
    </MemoryRouter>,
    {wrapper: TestWrapperNoRouter}
  );
  userEvent.click(screen.getByRole('button', {name: /log( )*out/i}));
  await screen.findAllByText('successfulRedirect');
});

