import { render, screen } from "@testing-library/react";
import React from 'react';
import Button from '../Button';
import {TestWrapper} from '../../testUtils';
import userEvent from "@testing-library/user-event";

test('Button calls onClick', () => {
  let beenClicked = false;
  render(
    <Button onClick={() => {beenClicked = true;}}>hello</Button>,
    {wrapper: TestWrapper}
  );
  userEvent.click(screen.getByRole('button'));
  expect(beenClicked).toBeTruthy();
});