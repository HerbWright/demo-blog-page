import React, { useState } from 'react';
import styled, {useTheme} from 'styled-components';
import Input from './Input';
import { tryLogin, isLoggedIn } from '../services/LoginApi';
import { useNavigate } from 'react-router-dom';
import Button from './Button';


const Form = styled.form`
  display: flex;
  flex-direction: column;
  row-gap: ${props => props.theme.spacingUnit};
`
const ErrorWrapper = styled.div`
  margin-top: calc(-${props => props.theme.spacingUnit} * 0.5);
  margin-bottom: 5px;
  height: 0;
  color: ${props => props.theme.warningFontColor};
`


function LoginForm() {
  const [username, setUsername] = useState('');
  const [password, setPassword] = useState('');
  const [error, setError] = useState('');
  const navigate = useNavigate();

  const onSubmit = e => {
    e.preventDefault();
    if(tryLogin(username, password)) {
      navigate('/');
    }
    else {
      setUsername('');
      setPassword('');
      setError('That login info is incorrect')
    }
  };
  
  return ( 
    <Form onSubmit={onSubmit}>
      <Input value={username} onChange={val => {setUsername(val.target.value)}} name='Username:' type='text'/>
      <Input aria-label='password' value={password} onChange={val => {setPassword(val.target.value)}} name='Password:' type='password'/>
      <ErrorWrapper>
        {error}
      </ErrorWrapper>
      <Button>Log In</Button>
      <input type="submit" style={{display: 'none'}} />
    </Form>
  );
}

export default LoginForm;