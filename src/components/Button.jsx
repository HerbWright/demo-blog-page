import React from 'react';
import styled, {useTheme} from 'styled-components';

const StyledButton = styled.button`
  background: ${props => props.theme.clickable.background};
  color: ${props => props.theme.clickable.fontColor};
  height: 2rem;
  padding: 0 15px;
  border-radius: 1rem;
  border: none;
  box-shadow: ${props => props.theme.boxShadowSmall};
  &:hover {
    cursor: pointer;
    background: ${props => props.theme.clickable.background + 'e0'};

  }

`

function Button({children, ...props}) {
  useTheme();
  return ( <StyledButton {...props}>{children}</StyledButton> );
}

export default Button;