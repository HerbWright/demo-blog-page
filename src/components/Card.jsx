import React from 'react';
import styled, {useTheme} from 'styled-components';

const CardWrapper = styled.div`
  width: ${props => props.width || 'auto'};
  background: pink;
  display: flex;
  flex-direction: column;
  min-width: 300px;
  min-height: 300px;
  padding: ${props => props.theme.spacingUnit};
  box-shadow: ${props => props.theme.boxShadow};
  ${props => props.style}
`

const HeaderWrapper = styled.div`
  min-height: 4rem;
  font-size: 2rem;
  font-weight: bold;
  display: flex;
  align-items: center;
  margin-bottom: ${props => props.theme.spacingUnit};
`


function Card({header, children, ...props}) {
  useTheme();
  return ( 
    <CardWrapper {...props}>
      <HeaderWrapper>
        {header}
      </HeaderWrapper>
      {children}
    </CardWrapper> 
  );
}

export default Card;