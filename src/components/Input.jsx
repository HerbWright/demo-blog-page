import React from 'react';
import styled from 'styled-components';

const InputWrapper = styled.div`
  display: grid;
  grid-template-rows: 1.5rem 1.5rem;
`
const Label = styled.label`
`
const StyledInput = styled.input`
  background: #ffffff80;
  border: none;
`

function Input(props) {
  return ( 
    <InputWrapper>
      <Label>{props.name}</Label>
      <StyledInput {...props}/>
    </InputWrapper>
  );
}

export default Input;