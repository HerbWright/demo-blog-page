import React, { useEffect, useState } from 'react';
import {getAllBlogs} from '../services/BlogsApi';
import BlogCard from './BlogCard';
import Card from './Card';

function BlogList() {
  const [blogs, setBlogs] = useState([]);

  const updateBlogs = async () => {
    const allBlogs = await getAllBlogs();
    setBlogs(allBlogs);
  }

  useEffect(() => {
    updateBlogs();
  }, []); 

  return (
    <>
      {blogs 
      ? blogs.map((blog, index) => 
        <BlogCard key={index} blog={blog}/>
      ) 
      : null}
    </> 
  );

}

export default BlogList;