import React from 'react';
import Card from './Card';
import LoginForm from './LoginForm';

function LoginCard(props) {
  return ( 
    <Card header='Login'>
      <LoginForm/>
    </Card>
  );
}

export default LoginCard;